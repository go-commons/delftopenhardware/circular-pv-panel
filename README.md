# Circular Solar Panel
**Developing a photovotlaic (PV) panel that can be built and maintained locally. For more info, vist https://biospheresolar.nl **

![Prototype](./V0.0/Images/Finished Prototype.jpg)

## Hello! :raised_hand:
Welcome to the Circular PV Panel repository! This is a research and development project initiated by [Siemen Brinksma](https://www.linkedin.com/in/siemen-brinksma-619226179/) at the TU Delft and Universiteit Leiden. The goal of the project is to develop a crystalline silicon solar panel that can be built, maintained and refurbished locally, leading to a lower levelized cost of electricity and higher sustainability over the lifetime of the module.

## The Problem :cloud:
Solar energy is awesome, but it's [not circular yet](https://www.futurebridge.com/industry/perspectives-energy/the-future-of-solar-panel-recycling-a-circular-economy-insight/). Solar panels are mostly [produced in China](https://solar.lowtechmagazine.com/2015/04/how-sustainable-is-pv-solar-power.html) and shipped around the world. The production of the silicon wafers is highly [energy intensive](https://www.researchgate.net/profile/EA_Alsema/publication/46689298_Life_Cycle_Analysis_of_Solar_Module_Recycling_Process/links/09e4150b7bfd9f2847000000/Life-Cycle-Analysis-of-Solar-Module-Recycling-Process.pdf) and they [cannot be recovered](https://www.sciencedirect.com/science/article/pii/S1364032117314065?casa_token=JLoJ3w6yjdIAAAAA:v0vTfaoJxvW-8E5MLQv8Qb2O5zO53SJZmKL9GFieMnbOHgl6s-wT2bBke6yTjhFU2F4SSJL7BQ) once a panel reaches its end-of-life. This is because during production the cells are soldered in place and laminated in EVA plastic to protect them against the elements. However, after 20-30 years in the field the lamination inevitably comes off and moisture [corrodes the contacts](https://www.sciencedirect.com/science/article/pii/S0038092X13002703?casa_token=hYg8dpU1qGgAAAAA:56pSAErds9p_f201E_UCtQUqLRxgPk1rjnapsRHvgW77ELby6nOmShnKEVkcOOPo3BKLHDmRrA). Once this happens the module cannot be repaired and is sent to the scrapyard. Currently only Europe has a proper [recycling scheme](http://www.pvcycle.org/) in place for PV panels. Still, the silicon wafers can only be recovered in pieces through thermal treatment and chemical leaching. 

## The Solution :sunny: 
This project is about developing an encapsulant for the solar cells that avoids lamination. Furthermore, the cells are not soldered together, making them easy to remove. The goal is to make the panel modular, repairable, and easy to build. The circular solar panel exists of 6 basic components:
1. **Back Sheet**: A sheet of durable and rigid material featuring indentations that snugly house all the other components. The backsheet can be engraved with an identification number linking to its build details and material passport.
2. **Cells**: Crystalline solar cells that can be ordered cheaply online.
3. **Wiring**: Solar tabbing wire, bus bar wire, bypass diodes, and a junction box connecting the cells. 
4. **Gasket**: A rubber gasket around the edge of the backsheet used to seal the panel.
5. **Front Sheet**: A transparent sheet of durable and rigid material fitting onto the back sheet, keeping all components in place. For commercial solar panels, low-iron high transmittance glass with anti-reflection and anti-soiling coatings is used. 
6. **Oxygen & Moisture absorber**: An absorption sachet that absorbs most moisture and oxygen present inside the module. This prevents degradation of electronic components.

This design is modular, making it easy to repair if one of the cells fails, and it will be easy to extract components and refurbish or reuse them at the end-of-life. Furthermore, this design can be made with only the basic materials and components, a CNC mill and a soldering iron. 

## Design Challenges :pushpin:
The first version of the prototype is finished. Ongoing challenges are:
- **Cell Fragility**: The photovoltaic cells used are monocrystalline silicon cells of 0.18mm thin. These are very fragile and easily break during soldering and (re-)assembly. In order to prevent this from happening the next version will give the cells a sturdy housing to protect them from breaking when being handled.
- **Solderless Contacts**: To make sure cells can be easily removed, the module will need solderless contacts between the cells. The housing mentioned under cell fragility will also feature this contact.
- **Inert Atmosphere**: the gas inside the module should be inert, meaning it does not degrade the components. Currently an oxygen and moisture absorber is used to remove oxygen and moisture once the module is sealed. This is the most simple and low-tech solution so far. However, it is not yet sure how well this works over time and what kind of underpressure it creates, and what the pressure differential will mean for the seal requirement. Furthermore, the sachets contain metal and plastic and are single-use. A biodegradable option would be preferrable.
- **Seal**: to prevent outside air and moisture from entering the module during its lifetime, a gasket is positioned around the edge of the module between the front- and backsheet. Research is required to design a seal that can keep the module airtight for at least 25 years.
- **Heat Transfer**: solar cells work at a higher efficiency when the temperature is lower. The current design creates a greenhouse effect inside the module as the plastic back sheet does not effectively conduct heat out of the module. Improving heat transfer away from the module will help increase its efficiency.
- **Aesthetic**: commercial solar panels are seen by many as ugly and undesirable. Redesigning the module from a functional point of view also offers the opportunity to redesign it from an aesthetic point of view. Making desirable and aesthetically pleasing solar panels will certainly help accellerate the transition towards renewable energy.

Currently, the first prototype has been built, documentation on this prototype can be found in the [V0.0](https://gitlab.com/go-commons/delftopenhardware/circular-pv-panel/-/tree/master/V0.0) folder. Main design parameters are strength, durability, electrical performance, cost, safety and aesthetic properties. The next sections will go over some open design choices:

### Module Layout
There are many ways to build a solar panel. First of all, there are many different sizes possible, from small tiles to door-sized panels. The right size for the prototype should be determined based on the desired power output, ease of (re)manufacturing, and ease of (re)installing. Second, there are different ways to connect the cells together. The most common is to wire cells in series as this increases the voltage at manageable current levels. Some solar panels include bypass diodes after a string of cells to overcome shading effects. The design of the layout should be done in a way that makes it very easy to assemble the panel, and creates a workable power output. An option could be to use interdigitated back contact (IBC) cells, but these will increase the cost of material.

### Producing the Back Sheet
The main component of the panel, holding everything together, is the back sheet. There are multiple ways to produce a flat sheet with indentations. One would be to 3D print the topography on top of an existing sheet, another would be to CNC mill the topography out of a sheet, another would be to laser cut sheets and stick them on top of each other. To increase circularity, it would be best if the material of the back sheet is recycled. This is why the current prototpe uses [recycled plastic](https://preciousplastic.com/starterkits/buy/sheets.html). For a larger production facility, injection moulding will be used. If the cells have their own dedicated housings, the back sheet will have to be less complex.

### Producing the Front Sheet
The transparent sheet on the front of the solar panel can be made of multiple materials. For commercial solar panels, low iron high transmittance glass is used with an anti-reflective coating and an anti-soiling coating. The question is if such materials are easily available to small-scale makers. Moreover, glass is expensive, heavy and fragile. An alternative option would be to use acryllic or polycarbonate with a similar anti-reflective and anti-soiling coating. However, these materials have less transmittance and plastic is subject to degradation when exposed to UV radiation. Applying a UV filter may be beneficial to the lifetime of the components without giving up too much capacity. 

### Creating an Inert Atmosphere and Seal
There are many ways to create a seal to protect the cells from the elements. Currently I am using an o-ring made of soft EPDM rubber. Silicone sealant is not an option because it cannot be easily recovered and reused after repair or remanufacturing. To create the inert atmosphere the simple options of using an oxygen and moisture absorber are applied. However, more complex solutions could be to decompress the module's interior, or to fill the module with an inert gas.

### Sourcing Cells
A surprising challenge is how hard it is to find locally produced components, mainly solar cells. I live in the Netherlands, and it is very hard to order solar cells outside of Chinese webshops. Although there are production facilities in Europe, they usually don't cater to small consumers. The Chinese webshops are cheap, but it's not very sustainable and the delivery times are very long. I guess not many people DIY their own solar panels so there is not a big market for DIY components.. yet :wink:






