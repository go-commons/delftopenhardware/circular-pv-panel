# Circular Solar Panel: How-to V0.0

## Hello!
Welcome to the instruction for building the first prototype of the circular solar panel. This how-to will cover all of the steps required to build the panel, and go over some improvements and lessons for the next prototype. This version is not yet working properly! See notes below for issues.

### Materials Needed
To build the panel, you will need the following items:
| Category | Description | Price indication <br>12V module |
|-|-|-|
| Front Sheet |  |  |
| Glass | Low iron, high transmittance tempered glass. <br>640*994*4mm | €1.27 |
| Electronic Components |  |  |
| Cells & Wiring | 24 FBC cells, 157*157mm. The kit includes <br>tabbing wire, bus bar wire and flux pens | €37.81 |
| Diodes | 2 6V bypass diodes | €0.90 |
| Junction Box | PV junction box with one diode | €5.00 |
| Seal |  |  |
| O-ring | EPDM sponge round chord ø4mm, 3268mm | €1.19 |
| Glue | A glue suited for EPDM | €0.05 |
| Backsheet & Fixation |  |  |
| Plastic sheet | Recycled plastic sheet, 794*1000*10mm  | €78.05 |
| Bolts & Nuts | 14 bolts and nuts, M5 x 25mm | €1.40 |
| Inert Atmosphere |  |  |
| Oxygen Absorber | 300cc oxygen absorber sachet | €1.70 |
| Moisture Absorber | Moisture absorber sachet | €1.00 |
|  | Total Price Indication | €125.67 |

Keep in mind that a the price is indicative and varies highly based on where you order components from and to and the quantity of the order.

### Tools Needed
To build the panel, you will need access to a CNC-milling machine, a laser cutter or jigsaw, a soldering iron, a drill and spanners.

### Step 1: Mill and cut the back sheet
In this step, the pockets for all other components are milled out of the back sheet. Additionally, the strips holding the glass in place will be cut and the holes to push the wiring through the backsheet to the junction box will be cut. 
Start by cutting two 18mm strips from the long edge and give the strips a 5mm indentation for each cell. These will later be used to hold the glass in place. Place the strips on the back sheet, like in the 3d model, and drill 7 5mm holes through the strip and the back sheet. These will be used to bolt the strip in place.

<img src="Images/Laser Cutting.png"  width="400" height="600">

Next, cut two 0.2*5mm slits at the top center of the back sheet. These will be used to push the bus bar wire to the back of the module where they can be connected to a junction box

Finally, use a CNC mill to mill the back sheet according to the [3d model](./V0.0 3D Model.stl) provided. At this point it is useful to already check whether the glass fits into the back sheet. If this is not the case, small adjustments can be made.


### Step 2: Install the wiring
In this step, the bus bar wires and back contact tabbing wires will be installed. The back contact tabbing wires will not be soldered to the cell in order to make it easy to remove a cell when needed. 

![Backsheet Bare](./Images/Backsheet bare.jpg)

Start by installing the bus bar wires at the bottom and the top of the module. Push about 5 cm extra bus bar wire through the slits at the top and close the slits with a sealant. Install the bypass diodes by soldering them to the bus bar wires in the designated pockets at the top of the module according to the circuit diagram. 

![Circuit Diagram](./Images/Circuit Diagram.png)

Next, cut 76 tabbing wires at a length of 160mm and 4 at a length of 170mm. The 170mm wires will be used to connect the bus bar wire to the back of the first cell in the series connection. The rest of the wires will be used to connect the back contact of the cells to the front contact of the next cell. The wire will reach until the space between the cells, where the front contact wire of the next cell will loosely touch it.
[PLAATJE]
Use a soldering iron to gently melt each wire onto the back sheet at three points which do not intersect with the cell's back contacts. Make sure the tabbing wires of each back contact do not connect as this would create a short circuit, bypassing the cell.

### Step 3: Pretab the cells
Once all the back contact wires are in place it is time to solder the tabbing wires onto the front of the cells. Cut 76 tabbing wires at a length of 160 mm and 4 at a length of 170 mm. The 170mm wires will be used to connect the front of the last cell in the series connection. To tab the cells, apply flux to the cell's front contact and tin the soldering iron. Be careful not to push the cell too hard wile soldering it as it might break. It is also a good idea to use something to hold the wires in place wile soldering as the position of the wire should align exactly with the contact. Tab the cells so that the tabbing wire sticks out about 3mm on one side.

### Step 4: Place the cells on the back sheet
Once all the cells are tabbed it is time to put them in place. The cells are wired according to the circuit diagram presented above.

![Interconnectons](./Images/Interconnections.jpg)

This means the front tabbing wire of each cell should align with the back tabbing wire of the next cell. If everything is done well it the wires align, if some wires do not align, cut away the extra plastic and use some extra tabbing wire to connect them.

### Step 5: Place the gasket on the back sheet
Cut the rubber round chord at an angle and place it in the designated pocket around the edge of the cell. Next, use some glue to connect the ends together.

### Step 6: Place the oxygen and moisture absorbers on the back sheet
Use the designated pockets to install the oxygen and moisture absorbers. You will have to lift up the cells to access the middle pocket. Make sure you are ready for the next step before you start this step as the absorbers will start immediately once they are removed from their packaging.

### Step 7: Place the glass on the back sheet
For this step you may want to get help from somebody as the glass is rather heavy and needs to be placed precisely into the hole without hitting the cells. Once the glass is in place, bolt the strips you cut in step 1 to the back sheet.

### Step 8: Install the junction box and test
Install the junction box at the back of the module by connecting it to the bus bar wires. Use a multimeter to test the voltage. Additionally you can use electroluminescence to test for cracks in the cells or inproper connections.

## Lessons & Improvements 
This design is not perfect, and along the way I learned a couple of things which will be incorporated into the next prototype

### Interconnections and Cell Housing
The cells are very fragile and working with them is tedious. Therefore, the next design will have dedicated housings for each cell which allow them to be handled with ease. The interconnection method between the cell housings will be solderless, meaning the cells can be placed and removed without soldering.

### Back sheet
The plastic back sheet of 10mm has several downsides. Because the material is too thin it is flexible, meaning the space between the backsheet and the glass is too large as the module flexes, and the connection between the cell and the back wire is lost. For this prototype I have resolved the issue by reinforcing the back with a wooden sheet. Another problem with plastic is that it is a good thermal insulator, meaning the module becomes like a greenhouse, trapping heat on the inside. This will significantly reduce the performance of the cells. The new design will not need such a strong back sheet as the cells will have individual housings. However, the head conducivity away from the module should be improved.

### Minor issues
Besides these major issues, there are some smaller things that should be improved, which I'll list here.
- The plastic strip holding down the front glass is currently to thick, meaning the module will be partially shaded under a low angle of irradiance. Alternatively, the border around the module can be made wider, which would also give room for a second seal if needed.
- There is currently no space around the border which can be used to lift the glass. The next design will have a small cutout so it becomes easier to lift the glass

Thank you for reading this documentation! Please contact me if you have any suggestions. See you in V0.1!

## License
Copyright Siemen Brinksma 2021.
This source describes Open Hardware and is licensed under the CERN-
OHL-S v2.

You may redistribute and modify this source and make products using it under the terms of the CERN-OHL-S v2 (https://ohwr.org/cern ohl s v2.txt ).

This source is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-S v2 for applicable conditions.

As per CERN-OHL-S v2 section 4, should you produce hardware based
on this source, You must where practicable maintain the Source Location visible on the back sheet of the Circular Solar Panel or other products you make using this source.
